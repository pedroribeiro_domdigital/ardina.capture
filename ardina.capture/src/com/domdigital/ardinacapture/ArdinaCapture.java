package com.domdigital.ardinacapture;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.OpenableColumns;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;

public class ArdinaCapture extends Activity {

	public static final String TAG = "ardina.capture";
	
	public static final String ACCESS_KEY_ID = "AKIAI5OJXZ4UL7FJONUQ";//"AKIAIFF2VRDK5AAUS4YA";//
	public static final String SECRET_KEY = "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";// "Z2pbfDxwdZv0dsS3G4K3DcjrsJZfj0aD2ltZ7lHX";//

	//http://ardina.capture.s3.amazonaws.com/
	public static final String BUCKET = "ardina.capture";
	//public static final String PICTURE_NAME = "capture";

	private static final int SELECTED = 1;
		
	private ImageButton selectPhoto = null;
	private ImageButton selectAudio = null;
	private ImageButton selectVideo = null;
	private ImageButton selectText = null;
	
	private ImageButton uploadPhoto = null;
	private TextView filePath = null;
	String filename = "";
	String mime_type = "";
	
	Uri selectedImage;
	
	AmazonS3Client s3Client = new AmazonS3Client(new BasicAWSCredentials(ACCESS_KEY_ID, SECRET_KEY));
	
	String rest_endpoint = "https://kiosk-developer-edition.na14.force.com/services/apexrest/kiosk/capture";
	
	//SAMPLE S3 UPLOADER
	//https://github.com/awslabs/aws-sdk-android-samples/blob/master/S3_Uploader/src/com/amazonaws/demo/s3uploader/S3UploaderActivity.java
	/*public static String getPictureBucket() {
		return (PICTURE_BUCKET);//"my-unique-name" + ACCESS_KEY_ID + PICTURE_BUCKET).toLowerCase(Locale.UK);
	}*/
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.i(TAG, "onCreate");
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ardina_capture);
		
		filePath = (TextView) findViewById(R.id.filePath);	
	    
		// Get intent, action and MIME type
	    Intent intent = getIntent();
	    String action = intent.getAction();
	    String type = intent.getType();
	    
	    selectPhoto = (ImageButton) findViewById(R.id.select_photo_btn);
		selectPhoto.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// Start the image picker.
				Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
				intent.setType("image/*");
				startActivityForResult(intent, SELECTED);
			}
		});
		selectAudio = (ImageButton) findViewById(R.id.select_audio_btn);
		selectAudio.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// Start the image picker.
				Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
				intent.setType("audio/*");
				startActivityForResult(intent, SELECTED);
			}
		});
		selectVideo = (ImageButton) findViewById(R.id.select_video_btn);
		selectVideo.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// Start the image picker.
				Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
				intent.setType("video/*");
				startActivityForResult(intent, SELECTED);
			}
		});
		selectText = (ImageButton) findViewById(R.id.select_text_btn);
		selectText.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// Start the image picker.
				Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
				intent.setType("application/pdf");
				startActivityForResult(intent, SELECTED);
			}
		});
		
		uploadPhoto = (ImageButton) findViewById(R.id.select_upload);
		uploadPhoto.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// Start the image picker.
				if(selectedImage!=null){
					Log.d(TAG, "URI: "+selectedImage);
					new S3PutObjectTask().execute(selectedImage);
				}
			}
		});
		
		if (Intent.ACTION_SEND.equals(action) && type != null) {
	    	if (type.startsWith("image/") || type.startsWith("audio/") || type.startsWith("video/") || type.startsWith("application/pdf")) {
	    		handleSendImage(intent);
	    	}
	    }
	    
	    
	}
	
	
	void updateUi(Uri iUri){
		selectedImage = iUri;
		
		ContentResolver cR = ArdinaCapture.this.getContentResolver();
		
		String tye = cR.getType(selectedImage);
		Log.e(TAG, "Type: "+tye);
		if(tye!=null && tye.contains("/"))
			mime_type = tye.split("/")[0];
		Log.e(TAG, "MIME TYPE:: "+mime_type);
		
		
		MimeTypeMap mime = MimeTypeMap.getSingleton();
		String filetype = mime.getExtensionFromMimeType(cR.getType(selectedImage));
		
		Log.e(TAG, "FILE TYPE:: "+filetype);
		
		filename = "capture_" + selectedImage.getLastPathSegment()+"."+filetype;
		Log.e(TAG, "FILENAME:: "+filename);
		
		filePath.setText(filename);
		filePath.setVisibility(View.VISIBLE);
		
		uploadPhoto.setVisibility(View.VISIBLE);		
	}
	
	
	void handleSendImage(Intent intent) {
	    Uri imageUri = (Uri) intent.getParcelableExtra(Intent.EXTRA_STREAM);
	    
	    if (imageUri != null) {
	        updateUi(imageUri);
	    }
	}
	
	// This method is automatically called by the image picker when an image is
	// selected.
	protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
		super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

		switch (requestCode) {
		case SELECTED:
			if (resultCode == RESULT_OK) {
				Uri u = imageReturnedIntent.getData();
				updateUi(u);					
			}
		}
	}
	
	private class S3PutObjectTask extends AsyncTask<Uri, Void, S3TaskResult> {

		ProgressDialog dialog;

		protected void onPreExecute() {
			
			try {
				dialog = new ProgressDialog(ArdinaCapture.this);
				dialog.setMessage(ArdinaCapture.this.getString(R.string.uploading));
				dialog.setCancelable(false);
				dialog.show();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		protected S3TaskResult doInBackground(Uri... uris) {

			if (uris == null || uris.length != 1) {
				return null;
			}

			// The file location of the image selected.
			Uri selectedImg = uris[0];

            ContentResolver resolver = getContentResolver();
            String fileSizeColumn[] = {OpenableColumns.SIZE}; 
            
			Cursor cursor = resolver.query(selectedImg, fileSizeColumn, null, null, null);

            cursor.moveToFirst();

            int sizeIndex = cursor.getColumnIndex(OpenableColumns.SIZE);
            
            // If the size is unknown, the value stored is null.  But since an int can't be
            // null in java, the behavior is implementation-specific, which is just a fancy
            // term for "unpredictable".  So as a rule, check if it's null before assigning
            // to an int.  This will happen often:  The storage API allows for remote
            // files, whose size might not be locally known.
            String size = null;
            if (!cursor.isNull(sizeIndex)) {
                // Technically the column stores an int, but cursor.getString will do the
                // conversion automatically.
                size = cursor.getString(sizeIndex);
            } 
            
			cursor.close();

			ObjectMetadata metadata = new ObjectMetadata();
			metadata.setContentType(resolver.getType(selectedImg));
			
			if(size != null){
			    metadata.setContentLength(Long.parseLong(size));
			}

			S3TaskResult result = new S3TaskResult();

			// Put the image data into S3.
			try {
				s3Client.setBucketAcl(BUCKET, CannedAccessControlList.PublicReadWrite);
				PutObjectRequest por = new PutObjectRequest( 
											BUCKET, 
											filename, 
											resolver.openInputStream(selectedImg), 
											metadata);
				por.withCannedAcl(CannedAccessControlList.PublicReadWrite);
				s3Client.putObject(por);
			} catch (Exception exception) {
				Log.e(TAG, exception.getMessage());
				result.setErrorMessage(exception.getMessage());
			}

			return result;
		}

		protected void onPostExecute(S3TaskResult result) {
			
			dialog.dismiss();

			if (result.getErrorMessage() != null) {

				displayErrorAlert(ArdinaCapture.this.getString(R.string.upload_failure_title),	result.getErrorMessage());
			} else{
				
				new SFPost().execute();
			} 
		}
	}
	
	private class SFPost extends AsyncTask<Void, Void, Integer>{

		protected Integer doInBackground(Void... params) {
			return Integer.valueOf(postData("http://"+BUCKET+".s3.amazonaws.com/"+filename, mime_type));			
		}
		
		protected void onPostExecute(Integer i) {
			int code = i;
			
			if(code == 200 ){
		    	Toast.makeText(ArdinaCapture.this, "Upload successfull", Toast.LENGTH_LONG).show();
		    	filePath.setText("");
		    	filePath.setVisibility(View.GONE);
		    	uploadPhoto.setVisibility(View.GONE);
		    }
		}
		
		
		
	}
	
	protected void displayErrorAlert(String title, String message) {

		AlertDialog.Builder confirm = new AlertDialog.Builder(this);
		confirm.setTitle(title);
		confirm.setMessage(message);

		confirm.setNegativeButton(
				ArdinaCapture.this.getString(R.string.ok),
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {

						ArdinaCapture.this.finish();
					}
				});

		confirm.show().show();
	}
	
	private class S3TaskResult {
		String errorMessage = null;
		Uri uri = null;

		public String getErrorMessage() {
			return errorMessage;
		}

		public void setErrorMessage(String errorMessage) {
			this.errorMessage = errorMessage;
		}

		public Uri getUri() {
			return uri;
		}

		public void setUri(Uri uri) {
			this.uri = uri;
		}
	}
	
	
	public int postData(String u, String t) {
		// Create a new HttpClient and Post Header
		HttpClient httpclient = new DefaultHttpClient();
		
		String endpoint = rest_endpoint + "?t="+t+"&u="+u;
		Log.i(TAG, "END:: "+endpoint);
		
		HttpPost post = new HttpPost(endpoint);
		
		post.setHeader("Content-type", "application/json");
		
		try {
			
			// Execute HTTP Post Request
		    HttpResponse response = httpclient.execute(post);
		    
		    Log.e(TAG, response.getStatusLine().getStatusCode()+"");
		    Log.e(TAG, response.getStatusLine().getReasonPhrase());

		    return response.getStatusLine().getStatusCode();
		    /*
		    if(response.getStatusLine().getStatusCode()== 200 ){
		    	Toast.makeText(ArdinaCapture.this, "Upload successfull", Toast.LENGTH_LONG).show();
		    }*/
		    
		} catch (Exception e) {
			Log.e(TAG, "ERROR::", e);
			return 0;
		}
	}

}
